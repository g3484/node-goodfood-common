import { Subjects } from './subjects';

export interface DeliveryCompletedEvent {
  subject: Subjects.DeliveryCompleted;
  data: {
    id: string;
    orderId: string;
  };
}
