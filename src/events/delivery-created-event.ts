import { Subjects } from './subjects';

export interface DeliveryCreatedEvent {
  subject: Subjects.DeliveryCreated;
  data: {
    id: string;
    orderId: string;
  };
}
