import { Subjects } from './subjects';

export interface DeliveryDeletedEvent {
  subject: Subjects.DeliveryDeleted;
  data: {
    id: string;
    orderId: string;
  };
}
