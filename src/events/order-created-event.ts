import { Subjects } from './subjects';
import { OrderStatus } from './types/order-status';

export interface OrderCreatedEvent {
  subject: Subjects.OrderCreated;
  data: {
    id: string;
    userId: string;
    userName: string;
    address: string;
    price: number;
    status: OrderStatus;
    dateOfOrder: Date;
    version: number;
  };
}
