import { Subjects } from './subjects';

export interface ProductUpdatedEvent {
  subject: Subjects.ProductUpdated;
  data: {
    id: string;
    version: number;
    name: string;
    description: string;
    imageUrl: string;
    publicIdImage: string;
    price: number;
    idCategory: string;
  };
}
