export enum DeliveryStatus {
  InProgress = 'in_progress',
  Completed = 'completed',
}
