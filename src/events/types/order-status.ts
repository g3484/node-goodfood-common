export enum OrderStatus {
  Created = 'created',
  Cancelled = 'cancelled',
  AwaitingPayment = 'awaiting_payment',
  AwaitingDelivery = 'awaiting_delivery',
  DeliveryInProgress = 'delivery_in_progress',
  Completed = 'completed',
}
